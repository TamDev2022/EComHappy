﻿global using Infrastructure.AutofacModules;
global using MediatR;
global using Autofac;
global using Autofac.Extensions.DependencyInjection;
global using Microsoft.IdentityModel.Tokens;
global using Microsoft.AspNetCore.Authentication.JwtBearer;
global using Microsoft.AspNetCore.Mvc.ApiExplorer;
global using Swashbuckle.AspNetCore.Filters;

