﻿namespace WebApi.ConfigurationOptions
{
    public class ConnectionStrings
    {
        public string ApplicationDbContext { get; set; }
    }
}
