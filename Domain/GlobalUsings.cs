﻿global using MediatR;
global using Domain.Base;
global using Domain.AggregatesModel.AudiLogEntryAggregate;
global using Domain.AggregatesModel.ProductAggregate;
global using Domain.AggregatesModel.RoleAggregate;
global using Domain.AggregatesModel.UserAggregate;
global using System.Data;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.ChangeTracking;

