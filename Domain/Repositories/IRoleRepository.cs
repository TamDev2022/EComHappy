﻿
namespace Domain.Repositories
{
    public interface IRoleRepository : IGenericRepository<Role>
    {
    }
}
